# Intro

A module is an independent process that communicates with other
modules using communication interfaces (IFC) implemented in `libtrap`.

![NEMEA parts](NEMEA-parts.png)


Real configuration - set of running modules is shown here, everything runs on a
single machine:

    --- [CONFIGURATION STATUS] ---
          | name                 | enabled | status  | PID   |
    Profile: Data sources        |  true   |
       0  | ipfixcol             |  true   | running | 6095  |
    
    Profile: Detectors           |  true   |
       1  | dns_amplification    |  true   | running | 31245 |
       2  | bruteforce_detector  |  true   | running | 31248 |
       3  | ddos_detector        |  true   | running | 31249 |
       4  | haddrscan_detector   |  true   | running | 31250 |
       5  | haddrscan_aggregator |  true   | running | 8631  |
       6  | hoststatsnemea       |  true   | running | 31252 |
       7  | ipblacklistfilter    |  true   | running | 31253 |
       8  | ipv6stats            |  true   | running | 31254 |
       9  | vportscan_detector   |  true   | running | 31257 |
       10 | vportscan_aggregator |  true   | running | 31258 |
    
    Profile: Reporters           |  true   |
       11 | hoststats2idea       |  true   | running | 8549  |
       12 | amplification2idea   |  true   | running | 8550  |
       13 | ipblacklist2idea     |  true   | running | 8551  |
       14 | vportscan2idea       |  true   | running | 8552  |
       15 | bruteforce2idea      |  true   | running | 8553  |
       16 | haddrscan2idea       |  false  | stopped | 0     |
       17 | ddos_detector2idea   |  true   | running | 8557  |
       20 | warden_filer         |  true   | running | 2255  |
    
    Profile: Munin               |  true   |
       18 | link_traffic         |  true   | running | 31281 |
       19 | proto_traffic        |  true   | running | 31288 |
    
    Profile: Others              |  true   |



It is recommended to go through the [demo/tutorial](tutorial.html).

You may continue reading documentation at http://nemea.liberouter.org/doc/


