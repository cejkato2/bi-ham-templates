# 1. CSV files

Look into source file (csv ~ comma separated value):

    less data-samples/lan-basic-flow.csv

Optionally, look at IoT data samples:

    less data-samples/virtual-sensor.csv

    less data-samples/zwave-sensor.csv

## Replay CSV files

    /usr/bin/nemea/logreplay -i f:outputfile.trapcap -f data-samples/lan-basic-flow.csv

## Translate UniRec into CSV

Logger will wait for data at `mysocket` IFC and translate it to CSV

    /usr/bin/nemea/logger -i u:mysocket -t

We can use `traffic_repeater` to replay UniRec stored in the file:

    /usr/bin/nemea/traffic_repeater -i f:outputfile.trapcap,u:mysocket:buffer=off

# 2. Generate own data

Have a look into `zwave-generator.py`:

    less zwave-generator.py

Generate data and show it using `logger` (ideally in 2 terminals):

    ./zwave-generator.py
    /usr/bin/nemea/logger -i u:mysocket -t


# 3. Store data into binary UniRec file(s)

`:w` rewrite

`:a` append (creates new file with suffix, e.g. data.trapcap.0, data.trapcap.1)

`:size=` in MB

`:time=` in minutes

    /usr/bin/nemea/traffic_repeater -i u:mysocket,f:soubor:a

    /usr/bin/nemea/traffic_repeater -i u:mysocket,f:soubor:time=2

Result:

    $ ls -lh soubor*
    -rw-rw-r--. 1 tomas tomas 929 26. Oct 23.38 soubor.201710262336
    -rw-rw-r--. 1 tomas tomas  47 26. Oct 23.38 soubor.201710262338
    $

# 4. Convert PCAP file into flow data

Prepare your PCAP to convert. Lets assume it is called `file.pcap`.

Additionally, lets assume we want CSV as the output.

Start `flow_meter` that will read the file and send flow records via
UNIX socket called `flowdata`:

    /usr/bin/nemea/flow_meter -r file.pcap -i u:flowdata:buffer=off

Start NEMEA logger to receive UniRec data via the UNIX socket and translate
them into CSV. Notice: `-t` will make `logger` to write header as well.

    /usr/bin/nemea/logger -t -i u:flowdata

# 5. Your own module

It is worth noting that your module can process UniRec directly instead
of conversion into CSV.
See examples in `/usr/share/doc/nemea/examples`, choose programming language
and start coding.

