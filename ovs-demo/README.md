# Setup

![Image]{./schema-infa-turris.drawio.png}

```
ip link a veth-at type veth peer name veth-oa
ip link a veth-srv type veth peer name veth-ovs
ip link set up dev veth-at
ip link set up dev veth-oa
ip link set up dev veth-ovs
ip link set up dev veth-srv

# optionally to get IPFIX data:
# ip a a 172.16.10.1/24 dev vboxnet0

vagrant up
```

# Configuration of the machines

* server: 172.16.10.2/24
* OVS: 172.16.10.254/24
* attacket: 172.16.10.123/24
* target of IPFIX (should be the host OS): 172.16.10.1:4739

# Troubleshooting

- Make sure you have vboxnet0 interface in host OS (create it using VirtualBox if missing)
- Make sure all needed NICs are up
- Make sure there are correctly set IP addresses everywhere, especially on OVS machine
