# ppexample

This code is an educational example of packet parsing.

`packets.h` file contains packets exported by wireshark - these packets will be
parsed by `parse_packet()` one by one.
The code expects ethernet header without any extension (such as VLAN), it
recognizes only IPv4 and TCP inside. If destination TCP port is 80, `Host:`
header is looked up.


## Building the code

Standard way can be used:

```
autoreconf -i
./configure
make
```

