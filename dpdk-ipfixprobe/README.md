# Description of Vagrantfile

Vagrantfile (for vagrantup.com) contains an example setup of VM with DPDK for experiments. There are two types of VMs differing by OS.

To create a machine, it is possible to execute:
* `vagrant up ubuntu` for *ubuntu/jammy64*
* `vagrant up oraclelinux` for *generic/oracle8*

VM has one management NIC by default from vagrant and Vagrantfile adds extra two NICs in hostonly or bridged modes.

:Warning: It is required to check names of network referenced interfaces (vboxnet0 and eno2) and update them according to the user's machine.

Note: the provisioning script installs dependencies, sets up DPDK, clones ipfixprobe and builds it.

An example how to start the DPDK ipfixprobe:

`./ipfixprobe -i 'dpdk;p=0;q=1;e=-a 0000:00:09.0' -p basic -o text`


