#ifndef MONLIB_KERNEL_HASHTABLE_H
#define MONLIB_KERNEL_HASHTABLE_H

#include <monlib_os.h>
void monlib_kernel_hashtable_init(struct monlib_os *os_ctx);
int monlib_kernel_hashtable_test(struct monlib_os *os_ctx);

#endif // MONLIB_KERNEL_HASHTABLE_H
