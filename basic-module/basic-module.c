#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>

MODULE_LICENSE("GPL");

static int __init basic_module_init(void) {
    printk(KERN_INFO "Init\n");
 return 0;
}
static void __exit basic_module_exit(void) {
    printk(KERN_INFO "Exit\n");
}
module_init(basic_module_init);
module_exit(basic_module_exit);
